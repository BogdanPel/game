package com.epam.controller;

import com.epam.model.units.Enemy;

public interface DungeonAnnotationController {
    void createEnemy();
}
