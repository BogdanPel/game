package com.epam.controller;

import com.epam.model.enums.CharacterClass;

public interface CharacterCreationController {
    void createCharacter(CharacterClass characterClass);

}
