package com.epam.controller.impl;

import com.epam.controller.CharacterCreationController;
import com.epam.model.enums.CharacterClass;
import com.epam.model.factory.CharacterFactory;
import com.epam.model.units.Character;
import com.epam.view.CharacterCreationView;

public class CharacterCreationControllerImpl implements CharacterCreationController {

    private Character character;
    private CharacterCreationView view;

    public CharacterCreationControllerImpl(CharacterCreationView characterCreationView) {
        view = characterCreationView;
    }

    public void createCharacter(CharacterClass characterClass) {
        CharacterFactory factory = new CharacterFactory(characterClass);
        character = factory.createCharacter();
        view.onCharacterCreated(character.getName());
//        Character.Builder builder = new Character.Builder();


    }
}
