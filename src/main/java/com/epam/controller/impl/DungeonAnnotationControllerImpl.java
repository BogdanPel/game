package com.epam.controller.impl;

import com.epam.controller.DungeonAnnotationController;
import com.epam.model.Model;
import com.epam.model.enums.EnemyClass;
import com.epam.model.factory.EnemyFactory;
import com.epam.model.units.Enemy;
import com.epam.model.utils.NumberParser;
import com.epam.view.DungeonAnnotationView;

public class DungeonAnnotationControllerImpl implements DungeonAnnotationController {
    private Enemy enemy;
    private DungeonAnnotationView view;
    private Model model;

    public DungeonAnnotationControllerImpl(DungeonAnnotationView dungeonAnnotationView) {
        view = dungeonAnnotationView;
        model = new NumberParser();
    }

    public void getEnemyDescription() {
        enemy.getDescription();

    }

    public void createEnemy() {
        EnemyClass enemyClass = model.takeEnemy();
        EnemyFactory factory = new EnemyFactory(enemyClass);
        enemy = factory.createEnemy();
        view.onFightStart(enemy.getName(),enemy.getDescription());

    }

}
