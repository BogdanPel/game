package com.epam;

import com.epam.view.DungeonAnnotationView;
import com.epam.view.impl.CharacterCreationViewImpl;
import com.epam.view.CharacterCreationView;
import com.epam.view.impl.DungeonAnnotationViewImpl;

public class Application {
  public static void main(String[] args) {
    CharacterCreationView characterCreationView = new CharacterCreationViewImpl();
    DungeonAnnotationView dungeonView = new DungeonAnnotationViewImpl();
    characterCreationView.showWelcome();
    characterCreationView.readInput();
    dungeonView.beforeFight();
  }
}