package com.epam.view.io.output;

public class Outputer {

  public void printGreetings() {
    System.out.println("Greetings, Adventurer!");
    System.out.println(
        "You are about to enter into dangerous place. But first... Choose your character:");
  }

  public void printCharacterList() {

    System.out.println("1) Warrior");
    System.out.println("2) Sorcerer");
    System.out.println("3) Hunter");
    System.out.println("4) Paladin");
    System.out.println("5) Priest");
  }

  public void printError() {
    System.out.println("Please choose valid option");
  }

  public void print(String name) {
    System.out.println("You chose " + name + "..."); /* dobavyty staty */
  }

  public void print(String name,String description) {
    System.out.println(
            "You have been engaged by "
                    + name
                    + " "
                    + description);
  }

  public void printDungeonEnter() {
    System.out.println(
        "You entered dark and dangerous place with strong enemies but priceless loot!");
  }
  public void printEnemyAttributes() {

  }
}
