package com.epam.view.io.input;

import com.epam.model.enums.CharacterClass;
import com.epam.view.io.output.Outputer;

import java.util.InputMismatchException;
import java.util.Scanner;

public class IOManager {

  private final Outputer outputer;
  private final InputParser inputParser;
  private final InputValidator inputValidator;

  {
    inputParser = new InputParser();
    inputValidator = new InputValidator();
    outputer = new Outputer();
  }

  public void printWelcome() {
    outputer.printGreetings();
    outputer.printCharacterList();
  }

  private CharacterClass verificationInput(int input) {
    boolean isValidInput = inputValidator.isCharacterValid(input);

    if (isValidInput) {
      return inputParser.parseInput(input);
    } else {
      outputer.printError();
      return takeInput();
    }
  }

  public CharacterClass takeInput() {
    try {
      Scanner scanner = new Scanner(System.in);
      int input = scanner.nextInt();
      return verificationInput(input);
    } catch (InputMismatchException e) {
      outputer.printError();
      return takeInput();
    }
  }
  public void printChosen(String name,String description) {
    outputer.print(name,description);
  }
  public void printChosen(String name) {
    outputer.print(name);
  }
  public void enterDungeon(){
    outputer.printDungeonEnter();
  }
}
