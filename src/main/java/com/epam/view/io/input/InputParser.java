package com.epam.view.io.input;

import com.epam.model.enums.CharacterClass;

class InputParser {
    CharacterClass parseInput(int input){
        CharacterClass characterClass;
        switch (input){
            case 1: characterClass = CharacterClass.WARRIOR; break;
            case 2: characterClass = CharacterClass.SORCERER; break;
            case 3: characterClass = CharacterClass.HUNTER; break;
            case 4: characterClass = CharacterClass.PALADIN; break;
            case 5: characterClass = CharacterClass.PRIEST; break;
            default: characterClass = CharacterClass.WARRIOR;
        }
        return characterClass;
    }

//    CharacterClass parseInput(String input){
//        CharacterClass characterClass;
//        switch (input){
//            case 1: characterClass = CharacterClass.WARRIOR; break;
//            case 2: characterClass = CharacterClass.SORCERER; break;
//            case 3: characterClass = CharacterClass.HUNTER; break;
//            case 4: characterClass = CharacterClass.PALADIN; break;
//            case 5: characterClass = CharacterClass.PRIEST; break;
//            default: characterClass = CharacterClass.WARRIOR;
//        }
//        return characterClass;
//    }
}
