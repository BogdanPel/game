package com.epam.view;

public interface DungeonAnnotationView {
    void beforeFight();
    void onFightStart(String name,String description);
}
