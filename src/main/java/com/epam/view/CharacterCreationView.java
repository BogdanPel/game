package com.epam.view;

public interface CharacterCreationView {

    void showWelcome();
    void readInput();
    void onCharacterCreated(String character);

}
