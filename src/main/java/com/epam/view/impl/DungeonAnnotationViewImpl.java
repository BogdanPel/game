package com.epam.view.impl;

import com.epam.controller.DungeonAnnotationController;
import com.epam.controller.impl.DungeonAnnotationControllerImpl;
import com.epam.view.DungeonAnnotationView;
import com.epam.view.io.input.IOManager;

public class DungeonAnnotationViewImpl implements DungeonAnnotationView {
    DungeonAnnotationController controller = new DungeonAnnotationControllerImpl(this);
    private final IOManager ioManager = new IOManager();
    public void beforeFight() {
        controller.createEnemy();
    }

    public void onFightStart(String name,String description) {
        ioManager.printChosen(name,description);

    }
}
