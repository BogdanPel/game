package com.epam.view.impl;

import com.epam.controller.CharacterCreationController;
import com.epam.controller.impl.CharacterCreationControllerImpl;
import com.epam.view.CharacterCreationView;
import com.epam.view.io.input.IOManager;

public class CharacterCreationViewImpl implements CharacterCreationView {

    private final IOManager ioManager = new IOManager();
    private final CharacterCreationController controller = new CharacterCreationControllerImpl(this);

    public void showWelcome() {
        ioManager.printWelcome();
    }

    public void readInput() {
        controller.createCharacter(ioManager.takeInput());
    }

    public void onCharacterCreated(String character) {
        ioManager.printChosen(character);
        ioManager.enterDungeon();
    }

}
