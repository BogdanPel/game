package com.epam.model.units;

public abstract class Creature {
    private Health health;
    private Damage damage;
    public Creature () {
        health = new Health(150,150);
        damage = new Damage(20,40);
    }
    public abstract String getName();

    private boolean isAlive() {
        return health.getCurrentHealth() > 0;
    }

}
