package com.epam.model.units;

public class Damage {
    private int damage;
    private int damageDispersion;
    public Damage(int damage, int damageDispersion) {
        this.damage = damage;
        this.damageDispersion = damageDispersion;
    }
    public int getDamage() {
        return damage;
    }

    public int getDamageDispersion() {
        return damageDispersion;
    }
}
