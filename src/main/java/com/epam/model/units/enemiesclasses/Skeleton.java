package com.epam.model.units.enemiesclasses;

import com.epam.model.units.Enemy;

public class Skeleton extends Enemy {

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public String getName() {
        return "Skeleton";
    }
}
