package com.epam.model.units.classes;

import com.epam.model.enums.CharacterClass;
import com.epam.model.units.Character;

public class Warrior extends Character {
    @Override
    public String getName(){
        return "Warrior";
    }
}
