package com.epam.model.units.classes;

import com.epam.model.units.Character;

public class Hunter extends Character {
    @Override
    public String getName() {
        return "Hunter";
    }
}
