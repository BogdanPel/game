package com.epam.model.units.classes;

import com.epam.model.units.Character;

public class Paladin extends Character {
    @Override
    public String getName() {
        return "Paladin";
    }
}
