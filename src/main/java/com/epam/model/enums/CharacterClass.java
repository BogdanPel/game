package com.epam.model.enums;

public enum CharacterClass {
    WARRIOR,
    SORCERER,
    HUNTER,
    PALADIN,
    PRIEST
}
