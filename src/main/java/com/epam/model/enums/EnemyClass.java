package com.epam.model.enums;

public enum EnemyClass {
    SKELETON,
    DEMON,
    GIANT,
    DRAGON
}
