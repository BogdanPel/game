package com.epam.model.utils;

import java.util.Random;

public class RandomGenerator {
  private final Random random;
  public RandomGenerator() {
      random = new Random();
    }
 public int generateNumber() {
     int min = 1;
     int max = 4;
    return random.nextInt((max - min) + 1) + min;
 }
}
