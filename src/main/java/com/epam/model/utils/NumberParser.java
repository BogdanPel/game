package com.epam.model.utils;

import com.epam.model.Model;
import com.epam.model.enums.EnemyClass;

public class NumberParser implements Model {
  private RandomGenerator random;

  public NumberParser() {
    random = new RandomGenerator();
  }

 public EnemyClass takeEnemy() {
    int number = random.generateNumber();
    return parseInt(number);
  }

  private EnemyClass parseInt(int number) {
    EnemyClass enemyClass;
    switch (number) {
      case 1:
        enemyClass = EnemyClass.SKELETON;
        break;
      case 2:
        enemyClass = EnemyClass.DEMON;
        break;
      case 3:
        enemyClass = EnemyClass.GIANT;
        break;
      case 4:
        enemyClass = EnemyClass.DRAGON;
        break;
      default:
        enemyClass = EnemyClass.SKELETON;
        break;
    }
    return enemyClass;
  }
}
