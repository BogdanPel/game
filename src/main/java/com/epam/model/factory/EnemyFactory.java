package com.epam.model.factory;

import com.epam.model.enums.EnemyClass;
import com.epam.model.units.Enemy;
import com.epam.model.units.enemiesclasses.Demon;
import com.epam.model.units.enemiesclasses.Dragon;
import com.epam.model.units.enemiesclasses.Giant;
import com.epam.model.units.enemiesclasses.Skeleton;

public class EnemyFactory {
    private EnemyClass enemyClass;
    public EnemyFactory(EnemyClass enemyClass) {
        this.enemyClass = enemyClass;
    }
    public Enemy createEnemy() {
        switch (enemyClass) {
            case SKELETON: return new Skeleton();
            case DEMON: return new Demon();
            case GIANT: return new Giant();
            case DRAGON: return new Dragon();
            default: return new Skeleton();
    }

    }

}
