package com.epam.model.factory;

import com.epam.model.enums.CharacterClass;
import com.epam.model.units.Character;
import com.epam.model.units.classes.*;

public class CharacterFactory {
    private final CharacterClass characterClass;
    public CharacterFactory(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }
    public Character createCharacter() {
        switch (characterClass) {
            case WARRIOR: return new Warrior();
            case SORCERER: return new Sorcerer();
            case HUNTER: return new Hunter();
            case PALADIN: return new Paladin();
            case PRIEST: return new Priest();
            default: return new Warrior();
        }
    }
}
