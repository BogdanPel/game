package com.epam.model;

import com.epam.model.enums.EnemyClass;

public interface Model {
    EnemyClass takeEnemy();
}
